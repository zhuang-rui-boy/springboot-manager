package com.company.project.entity;

import lombok.Data;
import lombok.experimental.Accessors;

@Data
@Accessors(chain = true)
public class StorageServiceEntity {

    private String title;

}
