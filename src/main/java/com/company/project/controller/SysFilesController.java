package com.company.project.controller;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.company.project.common.utils.DataResult;
import com.company.project.entity.StorageServiceEntity;
import com.company.project.entity.SysFilesEntity;
import com.company.project.service.SysFilesService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.apache.shiro.authz.annotation.Logical;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Set;


/**
 * 文件上传
 *
 * @author wenbin
 * @version V1.0
 * @date 2020年3月18日
 */
@RestController
@RequestMapping("/sysFiles")
@Api(tags = "文件管理")
public class SysFilesController {
    @Resource
    private SysFilesService sysFilesService;

    @ApiOperation(value = "新增")
    @PostMapping("/upload")
    @RequiresPermissions(value = {"sysFiles:add", "sysContent:update", "sysContent:add"}, logical = Logical.OR)
    public DataResult add(@RequestParam(value = "file") MultipartFile file, @RequestParam("service") String serviceName) {
        // 判断文件是否空
        if (file == null || file.getOriginalFilename() == null || "".equalsIgnoreCase(file.getOriginalFilename().trim())) {
            return DataResult.fail("文件为空");
        }
        return sysFilesService.saveFile(file, serviceName);
        // return sysFilesService.saveFile(file);
    }

    @ApiOperation(value = "上传")
    @PostMapping("/uploadPic")
    @RequiresPermissions("sysContent:add")
    public void uploadPic(@RequestParam(name = "editormd-image-file", required = true)
                       MultipartFile file,
                       HttpServletResponse response) throws IOException {
        // 加上 charset=utf-8， 防止响应中文乱码
        response.setHeader("Content-Type", "text/html; charset=utf-8");
        DataResult dataResult = sysFilesService.saveFile(file, "minio");
        Map data = (Map) dataResult.getData();
        response.getWriter().write( "{\"success\": 1, \"message\":\"上传成功\",\"url\":\"" +  data.get("src") + "\"}" );
        // return Dict.create().set("success", 1).set("message", "上传成功！").set("url", data.get("src"));
    }


    @ApiOperation(value = "删除")
    @DeleteMapping("/delete")
    @RequiresPermissions("sysFiles:delete")
    public DataResult delete(@RequestBody @ApiParam(value = "id集合") List<String> ids) {
        sysFilesService.removeByIdsAndFiles(ids);
        return DataResult.success();
    }

    @ApiOperation(value = "下载")
    @GetMapping("/download")
    @RequiresPermissions("sysFiles:download")
    public DataResult download(@RequestParam("url") @ApiParam(value = "url") String url
            , @RequestParam("service") @ApiParam(value = "serviceName") String serviceName
            , HttpServletResponse response) {
        sysFilesService.downloadFile(url, serviceName, response);
        return DataResult.success();
    }

    @ApiOperation(value = "查询分页数据")
    @PostMapping("/listByPage")
    @RequiresPermissions("sysFiles:list")
    public DataResult findListByPage(@RequestBody SysFilesEntity sysFiles) {
        IPage<SysFilesEntity> iPage = sysFilesService.page(sysFiles.getQueryPage(), Wrappers.<SysFilesEntity>lambdaQuery().orderByDesc(SysFilesEntity::getCreateDate));
        return DataResult.success(iPage);
    }

    @ApiOperation(value = "查询在线存储服务")
    @GetMapping("/queryStorageOnline")
    // @RequiresPermissions("sysFiles:list")
    public DataResult queryStorageOnline() {
        Set<String> storageServices = sysFilesService.queryStorageOnline();
        List<StorageServiceEntity> storageServiceEntities = new ArrayList<>();
        for (String storageService : storageServices) {
            StorageServiceEntity storageServiceEntity = new StorageServiceEntity().setTitle(storageService);
            storageServiceEntities.add(storageServiceEntity);
        }
        return DataResult.success(storageServiceEntities);
    }


}
