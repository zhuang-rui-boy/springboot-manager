package com.company.project.common.aop.aspect;

import com.alibaba.fastjson.JSONObject;
import com.company.project.common.utils.Constant;
import com.company.project.common.utils.TokenUtils;
import com.company.project.service.HttpSessionService;
import com.company.project.service.PermissionService;
import com.company.project.service.RedisService;
import com.company.project.service.RoleService;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Pointcut;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;
import java.util.List;
import java.util.Set;

@Aspect
@Component
public class ClearAndReloadCacheAspect {

    @Resource
    private HttpSessionService httpSessionService;
    @Resource
    private RoleService roleService;
    @Resource
    private PermissionService permissionService;
    @Value("${spring.redis.key.prefix.userToken}")
    private String userTokenPrefix;
    @Resource
    private RedisService redisService;

    /**
     * 切入点
     * 切入点,基于注解实现的切入点  加上该注解的都是Aop切面的切入点
     */
    @Pointcut("@annotation(com.company.project.common.aop.annotation.ClearAndReloadCache)")
    public void pointCut() {

    }

    /**
     * 环绕通知
     * 环绕通知非常强大，可以决定目标方法是否执行，什么时候执行，执行时是否需要替换方法参数，执行完毕是否需要替换返回值。
     * 环绕通知第一个参数必须是org.aspectj.lang.ProceedingJoinPoint类型
     *
     * @param proceedingJoinPoint
     */
    @Around("pointCut()")
    public Object aroundAdvice(ProceedingJoinPoint proceedingJoinPoint) {
        System.out.println("----------- 环绕通知 -----------");
        System.out.println("环绕通知的目标方法名：" + proceedingJoinPoint.getSignature().getName());
        // 获得token
        String token = TokenUtils.getToken();
        // 第一次删除
        // redisService.del(token);

        // 执行加入双删注解的改动数据库的业务 即controller中的方法业务
        Object proceed = null;
        try {
            proceed = proceedingJoinPoint.proceed();
        } catch (Throwable throwable) {
            throwable.printStackTrace();
        }
        // 等业务处理完（数据库增加或更新或删除操作）之后
        // 更新redis
        String userId = httpSessionService.getCurrentUserId();
        Set<String> permissions = permissionService.getPermissionsByUserId(userId);
        JSONObject currentSession = httpSessionService.getCurrentSession();
        currentSession.put(Constant.PERMISSIONS_KEY, permissions);
        List<String> roles = roleService.getRoleNames(userId);
        currentSession.put(Constant.ROLES_KEY, roles);
        String key = userTokenPrefix + token;
        // 更新token
        long redisTokenKeyExpire = redisService.getExpire(key);
        redisService.setAndExpire(key, currentSession.toJSONString(), redisTokenKeyExpire);
        // 第二次删除
        // 开一个线程 延迟1秒（此处是1秒举例，可以改成自己的业务）
        // 在线程中延迟删除  同时将业务代码的结果返回 这样不影响业务代码的执行
        // new Thread(() -> {
        //     try {
        //         Thread.sleep(1000);
        //         redisUtil.del(token);
        //         System.out.println("-----------1秒钟后，在线程中延迟删除完毕 -----------");
        //     } catch (InterruptedException e) {
        //         throw new BusinessException("redis删除key错误！");
        //     }
        // }).start();

        return proceed;// 返回业务代码的值
    }
}
