package com.company.project.common.utils;

import com.company.project.common.exception.BusinessException;
import com.company.project.common.exception.code.BaseResponseCode;
import org.apache.commons.lang3.StringUtils;

import javax.servlet.http.HttpServletRequest;

/**
 * 获取token工具类
 */
public class TokenUtils {

    public static String getToken() {
        // 获取request
        HttpServletRequest request = HttpContextUtils.getHttpServletRequest();
        // 从header中获取token
        String token = request.getHeader(Constant.ACCESS_TOKEN);
        // 如果header中不存在token，则从参数中获取token
        if (StringUtils.isEmpty(token)) {
            token = request.getParameter(Constant.ACCESS_TOKEN);
        }
        if (StringUtils.isEmpty(token)) {
            throw new BusinessException(BaseResponseCode.TOKEN_ERROR);
        }
        return token;
    }
}
