package com.company.project.common.utils;

/**
 * 文件路径处理工具类
 */
public class FilePathUtils {

    /**
     * 替换源文件路径后缀
     * @param sourcePath 源文件路径
     * @param suffix 替换的后缀
     * @return 替换后缀后的文件路径
     */
    public static String replaceFilePathSuffix(String sourcePath, String suffix) {
        return sourcePath.substring(0, sourcePath.lastIndexOf(".")) + "." + suffix;
    }
}
