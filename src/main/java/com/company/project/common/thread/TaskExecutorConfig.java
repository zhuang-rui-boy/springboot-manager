package com.company.project.common.thread;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;

@Configuration
@EnableAsync // 启用异步任务支持
public class TaskExecutorConfig {

    @Bean
    public ThreadPoolTaskExecutor convertTaskExecutor() {
        ThreadPoolTaskExecutor executor = new ThreadPoolTaskExecutor();
        executor.setCorePoolSize(10); // 核心线程数
        executor.setMaxPoolSize(20); // 最大线程数
        executor.setQueueCapacity(30); // 等待队列容量
        executor.setThreadNamePrefix("file-convert-"); // 线程名称前缀
        executor.initialize(); // 初始化线程池
        return executor;
    }

}
