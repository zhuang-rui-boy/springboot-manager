package com.company.project.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.company.project.common.utils.DataResult;
import com.company.project.entity.SysFilesEntity;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletResponse;
import java.util.List;
import java.util.Set;

/**
 * 文件上传 服务类
 *
 * @author wenbin
 * @version V1.0
 * @date 2020年3月18日
 */
public interface SysFilesService extends IService<SysFilesEntity> {

    DataResult saveFile(MultipartFile file, String uploadServiceName);

    DataResult saveFile(MultipartFile file);

    void removeByIdsAndFiles(List<String> ids);

    void downloadFile(String url, String serviceName, HttpServletResponse response);

    Set<String> queryStorageOnline();
}

