package com.company.project.service.impl;

import cn.hutool.core.date.DateUtil;
import cn.hutool.core.io.FileUtil;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.company.project.common.config.FileUploadProperties;
import com.company.project.common.exception.BusinessException;
import com.company.project.common.utils.DataResult;
import com.company.project.common.utils.DateUtils;
import com.company.project.entity.SysFilesEntity;
import com.company.project.mapper.SysFilesMapper;
import com.company.project.service.FileConvertService;
import com.company.project.service.SysFilesService;
import com.zhuang.fileconvert.api.Words2Pdf;
import com.zhuang.storage.properties.ObjectStoreProperties;
import com.zhuang.storage.strategy.context.StorageStrategyContext;
import org.apache.commons.io.FileUtils;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.stereotype.Service;
import org.springframework.util.DigestUtils;
import org.springframework.web.multipart.MultipartFile;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletResponse;
import java.io.File;
import java.io.IOException;
import java.util.*;

import static com.company.project.common.utils.Constant.IMAG_TYPES;
import static com.company.project.common.utils.FilePathUtils.replaceFilePathSuffix;

/**
 * 文件上传 服务类
 *
 * @author wenbin
 * @version V1.0
 * @date 2020年3月18日
 */
@EnableConfigurationProperties(FileUploadProperties.class)
@Service("sysFilesService")
public class SysFilesServiceImpl extends ServiceImpl<SysFilesMapper, SysFilesEntity> implements SysFilesService {
    @Resource
    private FileUploadProperties fileUploadProperties;
    @Resource
    private StorageStrategyContext storageStrategyContext;
    @Resource
    private Words2Pdf words2Pdf;
    @Resource
    private FileConvertService fileConvertService;
    @Resource
    private ObjectStoreProperties storeProperties;


    @Override
    public DataResult saveFile(MultipartFile file, String uploadServiceName) {
        try {
            String fileName = file.getOriginalFilename();
            // region 获取文件md5值 -> 获取文件后缀名 -> 生成相对路径
            String md5 = DigestUtils.md5DigestAsHex(file.getInputStream());
            String fileRelativePath = DateUtil.format(new Date(), "yyyyMMdd") + com.zhuang.storage.utils.FileUtils.SLASH + md5 + com.zhuang.storage.utils.FileUtils.SLASH + fileName;
            String url = storageStrategyContext.executeUploadStrategy(file, uploadServiceName);
            SysFilesEntity sysFilesEntity = new SysFilesEntity();
            sysFilesEntity.setFileName(fileName);
            sysFilesEntity.setFilePath(fileRelativePath);
            sysFilesEntity.setUrl(url);
            sysFilesEntity.setServiceName(uploadServiceName);
            String type = Objects.requireNonNull(FileUtil.getSuffix(fileName)).toLowerCase();
            if (IMAG_TYPES.contains(type) || "pdf".equals(type)) {
                sysFilesEntity.setPreviewUrl(sysFilesEntity.getUrl());
            } else {
                sysFilesEntity.setPreviewUrl(replaceFilePathSuffix(url, "pdf"));
            }
            this.save(sysFilesEntity);
            Map<String, String> resultMap = new HashMap<>();
            resultMap.put("src", url);
            // 异步转换
            fileConvertService.wordsToPDF(uploadServiceName, fileRelativePath, fileName, Objects.requireNonNull(FileUtil.getSuffix(fileName)).toLowerCase(), replaceFilePathSuffix(fileRelativePath, "pdf"));
            return DataResult.success(resultMap);
        } catch (Exception e) {
            throw new BusinessException("上传文件失败");
        }
    }

    @Override
    public DataResult saveFile(MultipartFile file) {
        // 存储文件夹
        String createTime = DateUtils.format(new Date(), DateUtils.DATEPATTERN);
        String newPath = fileUploadProperties.getPath() + createTime + File.separator;
        File uploadDirectory = new File(newPath);
        if (uploadDirectory.exists()) {
            if (!uploadDirectory.isDirectory()) {
                uploadDirectory.delete();
            }
        } else {
            uploadDirectory.mkdirs();
        }
        try {
            String fileName = file.getOriginalFilename();
            // id与filename保持一直，删除文件
            String fileNameNew = UUID.randomUUID().toString().replace("-", "") + getFileType(fileName);
            String newFilePathName = newPath + fileNameNew;
            String url = fileUploadProperties.getUrl() + "/" + createTime + "/" + fileNameNew;
            // 创建输出文件对象
            File outFile = new File(newFilePathName);
            // 拷贝文件到输出文件对象
            FileUtils.copyInputStreamToFile(file.getInputStream(), outFile);
            // 保存文件记录
            SysFilesEntity sysFilesEntity = new SysFilesEntity();
            sysFilesEntity.setFileName(fileName);
            sysFilesEntity.setFilePath(newFilePathName);
            sysFilesEntity.setUrl(url);
            this.save(sysFilesEntity);
            Map<String, String> resultMap = new HashMap<>();
            resultMap.put("src", url);
            return DataResult.success(resultMap);
        } catch (Exception e) {
            throw new BusinessException("上传文件失败");
        }
    }

    @Override
    public void removeByIdsAndFiles(List<String> ids) {
        List<SysFilesEntity> list = this.listByIds(ids);
        ObjectStoreProperties.ConfigEntity minio = storeProperties.getMinio();
        list.forEach(entity -> {
            // 如果之前的文件存在，删除
            final String url = entity.getUrl();
            final String previewUrl = entity.getPreviewUrl();
            storageStrategyContext.executeDeleteStrategy(entity.getServiceName(), url.replace(minio.getDomainUrl() + "/" + minio.getBucket() + "/", ""));
            storageStrategyContext.executeDeleteStrategy(entity.getServiceName(), previewUrl.replace(minio.getDomainUrl() + "/" + minio.getBucket() + "/", ""));

        });
        this.removeByIds(ids);

    }

    @Override
    public void downloadFile(String url, String serviceName, HttpServletResponse response) {
        ObjectStoreProperties.ConfigEntity minio = storeProperties.getMinio();
        String fileRelativePath = url.replace(minio.getDomainUrl() + "/" + minio.getBucket() + "/", "");
        try {
            storageStrategyContext.executeDownloadStrategy(serviceName, fileRelativePath, response);
        } catch (IOException e) {
            throw new BusinessException("文件下载失败！fileName:" + FileUtil.getName(fileRelativePath));
        }

    }

    @Override
    public Set<String> queryStorageOnline() {
        return storageStrategyContext.storageStrategyMap.keySet();
    }

    /**
     * 获取文件后缀名
     *
     * @param fileName 文件名
     * @return 后缀名
     */
    private String getFileType(String fileName) {
        if (fileName != null && fileName.contains(".")) {
            return fileName.substring(fileName.lastIndexOf("."));
        }
        return "";
    }
}