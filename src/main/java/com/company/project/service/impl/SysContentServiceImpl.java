package com.company.project.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.company.project.entity.SysContentEntity;
import com.company.project.mapper.SysContentMapper;
import com.company.project.service.SysContentService;
import org.apache.commons.lang3.StringUtils;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.springframework.stereotype.Service;

/**
 * 内容 服务类
 *
 * @author wenbin
 * @version V1.0
 * @date 2020年3月18日
 */
@Service("sysContentService")
public class SysContentServiceImpl extends ServiceImpl<SysContentMapper, SysContentEntity> implements SysContentService {


    @Override
    public void saveContent(SysContentEntity sysContent) {
        if (StringUtils.isNotEmpty(sysContent.getHtml())) {
            sysContent.setContent(htmlToPlainText(sysContent.getHtml()));
        }
        save(sysContent);
    }

    @Override
    public void updateContentById(SysContentEntity sysContent) {
        if (StringUtils.isNotEmpty(sysContent.getHtml())) {
            sysContent.setContent(htmlToPlainText(sysContent.getHtml()));
        }
        updateById(sysContent);
    }

    private String htmlToPlainText(String html) {
        Document parse = Jsoup.parse(html);
        // 提取纯文本
        StringBuilder plainText = new StringBuilder();
        for (Element element : parse.body().children()) {
            if (element.tagName().equals("p")) {
                // 处理 p 标签
                plainText.append(element.text()).append("\n");

                // 查找 p 标签中的 img 标签
                Elements imgs = element.getElementsByTag("img");
                for (Element img : imgs) {
                    plainText.append("[Image: ").append(img.attr("src")).append("]\n");
                }
            } else {
                // 处理其他标签
                plainText.append(element.text()).append("\n");
            }
        }

        System.out.println("Plain text: " + plainText);
        return plainText.toString();
    }

}