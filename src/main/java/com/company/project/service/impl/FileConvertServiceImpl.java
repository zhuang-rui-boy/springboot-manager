package com.company.project.service.impl;

import cn.hutool.core.io.FileUtil;
import cn.hutool.core.lang.Assert;
import com.company.project.common.exception.BusinessException;
import com.company.project.mapper.SysFilesMapper;
import com.company.project.service.FileConvertService;
import com.zhuang.fileconvert.api.Words2Pdf;
import com.zhuang.storage.strategy.context.StorageStrategyContext;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.fileupload.disk.DiskFileItem;
import org.apache.commons.fileupload.disk.DiskFileItemFactory;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.http.MediaType;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.commons.CommonsMultipartFile;

import javax.annotation.Resource;
import java.io.*;

@Service
@Slf4j
public class FileConvertServiceImpl implements FileConvertService {
    @Resource
    private Words2Pdf words2Pdf;
    @Resource
    private SysFilesMapper sysFilesMapper;
    @Resource
    private StorageStrategyContext storageStrategyContext;

    @Override
    @Async("convertTaskExecutor")
    public void wordsToPDF(String downloadServiceName, String fileRelativePath, String fileName, String type, String pdfRelativePath) {
        log.info("执行异步转换并上传预览文件....");
        try (InputStream inputStream = storageStrategyContext.downloadFile(downloadServiceName, fileRelativePath);
             ByteArrayOutputStream outputStream = new ByteArrayOutputStream()) {
            words2Pdf.words2Pdf(inputStream, type, outputStream);
            MultipartFile multipartFile = covertMultipartFile(new ByteArrayInputStream(outputStream.toByteArray()), FileUtil.mainName(fileName) + ".pdf");
            String previewUrl = storageStrategyContext.executeUploadStrategy(multipartFile, downloadServiceName, pdfRelativePath);
            if (StringUtils.isEmpty(previewUrl)) {
                throw new BusinessException("上传预览文件失败!, pdfRelativePath: " + pdfRelativePath);
            }
        } catch (IOException e) {
            throw new BusinessException("生成预览文件失败");
        }

    }

    public static MultipartFile covertMultipartFile(InputStream inputStream, String fileName) throws IOException {
        Assert.notNull(inputStream, "InputStream must not be null");
        Assert.notBlank(fileName, "fileName must not be null");
        DiskFileItemFactory fileItemFactory = new DiskFileItemFactory();
        DiskFileItem fileItem = (DiskFileItem) fileItemFactory.createItem("file",
                MediaType.APPLICATION_PDF_VALUE, true, fileName);
        try (OutputStream outputStream = fileItem.getOutputStream()) {
            IOUtils.copy(inputStream, outputStream);
            fileItem.getOutputStream().flush();
            IOUtils.closeQuietly(inputStream);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
        if (fileItem.getSize() == 0) {
            throw new IOException("文件读取失败");
        }
        return new CommonsMultipartFile(fileItem);
    }


}
