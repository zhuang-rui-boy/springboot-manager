package com.company.project.service;

public interface FileConvertService {

    void wordsToPDF(String downloadServiceName, String fileRelativePath, String fileName, String type, String pdfRelativePath);

}
