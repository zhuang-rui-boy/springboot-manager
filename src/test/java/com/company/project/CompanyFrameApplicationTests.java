package com.company.project;

import com.alibaba.fastjson.JSON;
import com.company.project.common.utils.RedisUtil;
import com.company.project.entity.BaseEntity;
import com.company.project.service.RedisService;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import javax.annotation.Resource;

@RunWith(SpringRunner.class)
@SpringBootTest
public class CompanyFrameApplicationTests {
    @Resource
    private RedisUtil redisUtil;
    @Resource
    private RedisService redisService;

    @Test
    public void contextLoads() {
        final BaseEntity baseEntity = new BaseEntity();
        ObjectMapper objectMapper = new ObjectMapper();
        redisService.set("redisService", JSON.toJSONString(baseEntity));
        // redisUtil.set("setredisUtils", baseEntity);
        final BaseEntity baseEntity1 = (BaseEntity) redisUtil.get("redisUtils");
        final BaseEntity baseEntity2 = JSON.parseObject(redisService.get("redisService"), BaseEntity.class); ;
        System.out.println(baseEntity2);
        System.out.println(baseEntity1);
    }


}
